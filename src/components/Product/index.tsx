import { styled } from "styled-components";
import Button from "@packages/design-system/Button";
import { useModal } from "@packages/design-system/Modal";
import Cart from "@components/Cart";
import Image from "@packages/design-system/Image";
import BicycleSvg from "@packages/icons/bicycle.svg";
import { defaultCurrency, initialProductAmount } from "@constants/app";
import AmountOutput from "@packages/design-system/AmountOutput";

const Wrapper = styled.div``;
const ProductDetails = styled.div``;

const ProductImage = styled(Image)`
  width: 10em;
  height: 10em;
`;

const Placeholder = styled.div``;
const Price = styled.h3``;

export default function Product() {
  const { openModal, closeModal, isVisible } = useModal();

  return (
    <Wrapper className="flex flex-row justify-center mt-10 b-c">
      <ProductImage src={BicycleSvg} />
      <ProductDetails className="flex flex-col">
        <Placeholder className="bg-gray-400 h-5 w-full mt-2" />
        <Placeholder className="bg-gray-300 h-5 w-20 mt-2" />
        <Placeholder className="bg-gray-100 h-5 w-10 mt-2" />
        <Placeholder className="bg-gray-200 h-5 w-8 mt-2" />
        <Price className="font-bold text-gray-700 mt-2 mb-4">
          <AmountOutput amount={initialProductAmount} currency={defaultCurrency} />
        </Price>
        <Button text="ADD TO CART" onClick={openModal} />
      </ProductDetails>
      {isVisible && <Cart onClose={closeModal} />}
    </Wrapper>
  );
}
