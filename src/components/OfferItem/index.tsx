import { styled } from "styled-components";
import Button from "@packages/design-system/Button";
import Image from "@packages/design-system/Image";
import PlusSvg from "@packages/icons/plus.svg";
import MinusSvg from "@packages/icons/minus.svg";
import AmountOutput from "@packages/design-system/AmountOutput";
import Select from "@packages/design-system/Select";
import { Currency, Offer } from "@types";

const Wrapper = styled.div``;

const OfferImage = styled(Image)``;
const Title = styled.h3``;

const Description = styled.h4``;
const Container = styled.div``;
const OriginalPrice = styled.h2``;
const DiscountedPrice = styled.h5``;

interface OfferItemProps extends Offer {
  currency: Currency
  onAdd: () => void
  onRemove: () => void
  isAdded: boolean
}

export default function OfferItem({
  currency,
  title,
  short_description,
  image,
  discounted_price,
  original_price,
  onAdd,
  onRemove,
  isAdded,
}: OfferItemProps) {
  const btnClass = isAdded && "bg-red-500 hover:bg-red-700";
  return (
    <Wrapper className="flex flex-row justify-between items-center">
      <OfferImage src={image} className="w-20 h-20" />
      <Container className="flex flex-col flex-1 justify-center pl-4">
        <Container className="flex flex-row">
          <Title className="mr-2 font-bold">{title}</Title>
          {!!discounted_price && (
            <DiscountedPrice className="mr-2 text-gray-400 line-through">
              <AmountOutput amount={discounted_price} currency={currency} />
            </DiscountedPrice>
          )}
          <OriginalPrice className="flex">
            <AmountOutput amount={original_price} currency={currency} />
          </OriginalPrice>
        </Container>
        <Description>{short_description}</Description>
      </Container>
      <Select
        name="offer_variant"
        options={[
          {
            label: "Regular",
            value: "regular",
          },
        ]}
        value="regular"
      ></Select>
      <Button
        iconLeft={isAdded ? MinusSvg : PlusSvg}
        text={isAdded ? "Remove" : "Add"}
        onClick={isAdded ? onRemove : onAdd}
        className={`w-28 ${btnClass}`}
      ></Button>
    </Wrapper>
  );
}