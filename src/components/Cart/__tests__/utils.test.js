import { expect, test } from "vitest";
import { getCartAmount } from "../utils";
import { offersApiResponseStub } from "@packages/stubs/offers";
import { initialProductAmount } from "@constants/app";

test("components > Cart > utils > getCartAmount (with all offers)", () => {
  expect(getCartAmount(offersApiResponseStub.offers)).toBe(57.96);
});

test("components > Cart > utils > getCartAmount (with no offers, only dummy product price available)", () => {
  expect(getCartAmount([])).toBe(initialProductAmount);
});

test("components > Cart > utils > getCartAmount (with 2 offers)", () => {
  const [firstOffer, secondOffer] = offersApiResponseStub.offers;
  expect(getCartAmount([firstOffer, secondOffer])).toBe(52.97);
});
