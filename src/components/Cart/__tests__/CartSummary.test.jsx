import { expect, test } from "vitest";
import renderer from "react-test-renderer";
import CartSummary from "../CartSummary";

test("components > Cart > CartSummary > render correctly with currency and amount", () => {
  const component = renderer.create(
    <CartSummary currency="USD" amount={10.22} />
  );
  expect(component).toMatchSnapshot();
});

test("components > Cart > CartSummary > render correctly with no amount", () => {
  const component = renderer.create(<CartSummary amount={null}/>);
  expect(component).toMatchSnapshot();
});
