import { expect, test, vitest } from "vitest";
import renderer, { act } from "react-test-renderer";
import Cart from "..";
import { getOffers } from "@services/cart.service";
import { offersApiResponseStub } from "@packages/stubs/offers";
import OfferItems from "@components/OfferItems";
import CartSummary from "../CartSummary";

vitest.mock("@services/cart.service", () => ({
  getOffers: vitest.fn(),
}));

const props = {
  onClose: vitest.fn(),
};

test("components > Cart > index > render correctly with stubs", async () => {
  getOffers.mockReturnValueOnce(offersApiResponseStub);
  let component;
  await act(() => {
    component = renderer.create(<Cart {...props} />);
  });
  expect(component).toMatchSnapshot();
});

test("components > Cart > index > render correctly when we add and remove offers", async () => {
  getOffers.mockReturnValueOnce(offersApiResponseStub);
  let component;
  await act(() => {
    component = renderer.create(<Cart {...props} />);
  });

  const [firstOffer, secondOffer] = offersApiResponseStub.offers
  const offerItems = component.root.findByType(OfferItems);
  offerItems.props.onOfferAdd(firstOffer);
  offerItems.props.onOfferAdd(secondOffer);
  const cartSummary = component.root.findByType(CartSummary);
  expect(cartSummary.props.amount).toEqual(52.97);
  expect(cartSummary.props.currency).toEqual(offersApiResponseStub.currency);
  offerItems.props.onOfferRemove(1);
  expect(cartSummary.props.amount).toEqual(44.98);
});
