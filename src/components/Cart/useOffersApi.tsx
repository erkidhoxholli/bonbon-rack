import { defaultCurrency } from "@constants/app";
import { useEffect, useState } from "react";
import { getOffers as getOffersService } from "@services/cart.service";
import { Currency, Offer } from "@types";

interface OffersApiResponse {
  offers: Offer[];
  isLoading: boolean;
  currency: Currency;
}
export default function useOffersApi() {
  const [offersApiData, setOffersApiData] = useState<OffersApiResponse>({
    offers: [],
    isLoading: false,
    currency: defaultCurrency,
  });

  useEffect(() => {
    const abortController = new AbortController();

    const getOffers = async () => {
      setOffersApiData((prev) => ({ ...prev, isLoading: true }));
      const { offers, currency } = await getOffersService(
        abortController.signal
      );
      setOffersApiData({ offers, currency, isLoading: false });
    };

    getOffers();

    return () => {
      abortController.abort();
    };
  }, []);

  return offersApiData;
}
