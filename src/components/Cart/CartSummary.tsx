import Image from "@packages/design-system/Image";
import { styled } from "styled-components";
import CartSvg from "@packages/icons/cart.svg";
import AmountOutput from "@packages/design-system/AmountOutput";
import { Currency } from "@types";
import { defaultCurrency } from "@constants/app";

const Wrapper = styled.div``;
const Title = styled.h2``;
const Amount = styled.h2``;


interface CartSummaryProps {
  amount?: number
  currency?: Currency
}

export default function CartSummary({ amount, currency }: CartSummaryProps) {
  return (
    <Wrapper className="flex flex-row items-center justify-between bg-green-200 p-5">
      <Image src={CartSvg} className="h-8 mr-5" />
      <Title className="font-bold flex-1">Your cart</Title>
      {amount && (
        <Amount className="font-bold">
          <AmountOutput amount={amount} currency={currency || defaultCurrency} />
        </Amount>
      )}
    </Wrapper>
  );
}
