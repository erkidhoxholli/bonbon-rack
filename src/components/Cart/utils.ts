import { initialProductAmount } from "@constants/app";
import { Offer } from "@types";

export const getCartAmount = (activeOffers: Offer[]) => {
  const cartAmount = activeOffers.reduce((acc, curr) => {
    if (curr.discounted_price) {
      return (acc += curr.discounted_price);
    }
    return (acc += curr.original_price);
  }, initialProductAmount);

  return Number(cartAmount.toFixed(2));
};
