import { styled } from "styled-components";
import Modal from "@packages/design-system/Modal";
import Button from "@packages/design-system/Button";
import CartSummary from "./CartSummary";
import OfferItems from "@components/OfferItems";
import AnchorRightSvg from "@packages/icons/anchor-right.svg";
import { useCallback, useMemo, useState } from "react";
import { getCartAmount } from "./utils";
import useOffersApi from "./useOffersApi";
import { Offer } from "@types";

const ModalWrapper = styled(Modal)``;
const Header = styled.h3``;
const Subheader = styled.div``;

interface CartProps {
  onClose: () => void
}

export default function Cart({ onClose }: CartProps) {
  const [activeOffers, setActiveOffers] = useState<Offer[]>([]);
  const { offers, isLoading, currency } = useOffersApi();

  const onOfferAdd = useCallback((offer: Offer) => {
    setActiveOffers((prev: Offer[]) => [...prev, offer]);
  }, []);

  const onOfferRemove = useCallback((offerId: number) => {
    setActiveOffers((prev) => prev.filter(({ id }) => id !== offerId));
  }, []);

  const onContinueToCheckout = useCallback(() => {
    onClose();
    const activeOffersIds = activeOffers.map(({ id }) => id);
    alert(`Selected ids: ${activeOffersIds.join(", ")}`);
  }, [activeOffers, onClose]);

  const cartAmount = useMemo(() => getCartAmount(activeOffers), [activeOffers]);

  return (
    <ModalWrapper
      subheader={
        <Subheader>
          <CartSummary currency={currency} amount={cartAmount} />
        </Subheader>
      }
      header={
        <Header className="flex-1 text-center text-lg font-bold pt-2 pb-2">
          Wait, don&lsquo;t miss our deals, today only!
        </Header>
      }
      footer={
        <Button
          text="Continue to checkout"
          iconRight={AnchorRightSvg}
          onClick={onContinueToCheckout}
        />
      }
      onClose={onClose}
    >
      {isLoading && <div className="h-52">loading</div>}
      {!isLoading && (
        <OfferItems
          activeOffers={activeOffers}
          onOfferRemove={onOfferRemove}
          onOfferAdd={onOfferAdd}
          data={offers}
          currency={currency}
        />
      )}
    </ModalWrapper>
  );
}