import Image from "@packages/design-system/Image";
import BonbonSvg from "@packages/icons/bonbon.svg";
import { styled } from "styled-components";

const Wrapper = styled.nav``;

const Logo = styled(Image)`
  width: 2em;
  height: 2em;
`;

const Text = styled.h1``;

export default function Navbar() {
  return (
    <Wrapper className="flex justify-center items-center p-5 bg-green-100">
      <Logo src={BonbonSvg} />
      <Text className="ml-5 text-xl">Bonbon Rack</Text>
    </Wrapper>
  );
}
