import { styled } from "styled-components";
import OfferItem from "@components/OfferItem";
import PropTypes from "prop-types";
import { Currency, Offer } from "@types";

const Wrapper = styled.div``;

interface OfferItemsProps {
  activeOffers: Offer[]
  onOfferRemove: (offerId: number) => void
  onOfferAdd: (offer: Offer) => void
  data: Offer[]
  currency: Currency
}

export default function OfferItems({
  activeOffers,
  onOfferRemove,
  onOfferAdd,
  data,
  currency,
}: OfferItemsProps) {
  return (
    <Wrapper>
      {data.length === 0 && <div>No offers available</div>}
      {data.length > 0 && data.map((item) => (
        <OfferItem
          {...item}
          currency={currency}
          isAdded={!!activeOffers.find(({ id }) => id === item.id)}
          key={item.id}
          onRemove={() => onOfferRemove(item.id)}
          onAdd={() => onOfferAdd(item)}
        />
      ))}
    </Wrapper>
  );
}

OfferItems.propTypes = {
  activeOffers: PropTypes.array,
  onOfferRemove: PropTypes.func,
  onOfferAdd: PropTypes.func,
  currency: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      short_description: PropTypes.string.isRequired,
      original_price: PropTypes.number.isRequired,
      discounted_price: PropTypes.number,
      image: PropTypes.string.isRequired,
    })
  ),
};
