import { styled } from "styled-components";
import PropTypes from "prop-types";
import ImagePlaceholderSvg from "@packages/icons/image-placeholder.svg";

const Wrapper = styled.img<{$isIcon: boolean}>`
  ${({ $isIcon }) =>
    $isIcon &&
    `
    width: 1em;
    height: 1em;
  `}
`;

export interface ImageProps {
  src: string 
  className?: string
  isIcon?: boolean
}

export default function Image({ src, className, isIcon = false }: ImageProps) {
  return (
    <Wrapper
      className={`object-cover ${className}`}
      $isIcon={isIcon}
      src={src || ImagePlaceholderSvg}
      onError={(evt: any) => {
        evt.target.onerror = null
        evt.target.src = ImagePlaceholderSvg
      }}
    ></Wrapper>
  );
}

Image.propTypes = {
  src: PropTypes.node.isRequired,
  className: PropTypes.string,
  isIcon: PropTypes.bool,
};
