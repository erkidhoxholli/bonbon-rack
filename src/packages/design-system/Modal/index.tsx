import styled from "styled-components";
import PropTypes from "prop-types";
import { useModal as useModalHook } from "./useModal";
import Image from "@packages/design-system/Image";
import CloseSvg from "@packages/icons/close.svg";
import React from "react";

const Wrapper = styled.div`
  z-index: 1;
`;

const OuterContainer = styled.div``;

const InnerWrapper = styled.div`
  min-width: 50%;
  max-width: 80%;
`;

const Header = styled.div``;
const Subheader = styled.div``;

const Content = styled.div``;

const Footer = styled.div``;
const CloseButton = styled.button``;

export interface ModalProps extends React.HtmlHTMLAttributes<HTMLDivElement> {
  children: React.ReactNode
  header?: React.ReactNode
  subheader?: React.ReactNode
  footer?: React.ReactNode
  onClose?: () => void
}

export default function Modal({
  children,
  header,
  subheader,
  footer,
  onClose,
  ...props
}: ModalProps) {
  return (
    <Wrapper
      {...props}
      className="fixed -z-10 flex left-0 right-0 bottom-0 top-0 justify-center items-center text-gray-800"
    >
      <OuterContainer
        className="absolute bg-slate-500 opacity-80 h-full w-full"
        onClick={onClose}
      />
      <InnerWrapper className="rounded-md relative bg-white p-0">
        {header && (
          <Header className="flex justify-between items-center p-3">
            {header}
            <CloseButton onClick={onClose} className="hover:cursor-pointer">
              <Image isIcon src={CloseSvg} />
            </CloseButton>
          </Header>
        )}
        {subheader && <Subheader>{subheader}</Subheader>}
        <Content className="text-sm p-4">{children}</Content>
        {footer && <Footer className="flex justify-end p-3 border-t border-gray-300">{footer}</Footer>}
      </InnerWrapper>
    </Wrapper>
  );
}

export const useModal = useModalHook;

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node,
  header: PropTypes.node,
  subheader: PropTypes.node,
  footer: PropTypes.node,
};
