import { useState } from "react";

export interface IUseModal {
  isVisible: boolean;
  openModal: () => void;
  closeModal: () => void;
}
export function useModal(): IUseModal {
  const [isVisible, setIsVisible] = useState<boolean>(false);
  const openModal = () => {
    setIsVisible(true);
  };
  const closeModal = () => {
    setIsVisible(false);
  };

  return { isVisible, openModal, closeModal };
}
