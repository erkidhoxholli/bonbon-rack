import PropTypes from "prop-types";
import { OptionHTMLAttributes, SelectHTMLAttributes } from "react";
import { styled } from "styled-components";

const Wrapper = styled.div``;
const SelectBase = styled.select<SelectHTMLAttributes<HTMLSelectElement>>``;
const Option = styled.option<OptionHTMLAttributes<HTMLOptionElement>>``;

export interface SelectProps {
  name: string;
  options: { value: string; label: string }[];
  value: string;
}

export default function Select({ name, options, value }: SelectProps) {
  return (
    <Wrapper className="mr-2 h-10">
      <SelectBase
        defaultChecked
        id={name}
        name={name}
        defaultValue={value}
        className="border h-10 border-green-300 text-gray-900 text-sm rounded-md ring-green-300 focus-visible:ring-green-500 focus:ring-green-500 focus:border-green-500 block w-full p-2"
      >
        {options.map((option) => (
          <Option
            key={option.value}
            value={option.value}
          >
            {option.label}
          </Option>
        ))}
      </SelectBase>
    </Wrapper>
  );
}

Select.propTypes = {
  value: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
};
