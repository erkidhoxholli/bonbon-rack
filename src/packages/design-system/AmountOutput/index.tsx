import { defaultCurrency } from "@constants/app";
import { Currency } from "@types";

export interface IAmountOutput {
  amount: number;
  currency: Currency;
}

export default function AmountOutput({
  amount,
  currency = defaultCurrency,
}: IAmountOutput) {
  const formattedAmount = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: currency,
  }).format(amount);
  return formattedAmount;
}
