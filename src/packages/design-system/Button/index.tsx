import { styled } from "styled-components";
import Image from "@packages/design-system/Image";

const Wrapper = styled.button``;
const Icon = styled(Image)``;

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  text: string;
  className?: string;
  iconLeft?: string;
  iconRight?: string;
}

export default function Button({
  text,
  iconLeft,
  iconRight,
  className,
  ...rest
}: ButtonProps) {
  return (
    <Wrapper
      className={`flex flex-row justify-center items-center rounded-md bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 h-10 ${className}`}
      {...rest}
    >
      {!!iconLeft && <Icon className="invert mr-1 w-4 h-4" src={iconLeft} />}
      {text}
      {!!iconRight && <Icon className="invert mr-1 w-4 h-4" src={iconRight} />}
    </Wrapper>
  );
}
