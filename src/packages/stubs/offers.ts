export const offersApiResponseStub = {
  currency: "USD",
  offers: [
    {
      id: 1,
      title: "Bulb",
      short_description: "You know you will need them later",
      original_price: 9.98,
      discounted_price: 7.99,
      image:
        "https://cdn.shopify.com/s/files/1/0078/4606/8295/products/black_medium.png?v=1565111862",
    },
    {
      id: 2,
      title: "Gift packaging",
      short_description: "We will wrap your order in style",
      original_price: 4.99,
      discounted_price: null,
      image:
        "https://candyrack-staging.digismoothie.app/static/media/gift.eddd5f8e.svg",
    },
    {
      id: 3,
      title: "First in line",
      short_description: "Priority order processing",
      original_price: 4.99,
      discounted_price: null,
      image:
        "https://candyrack-staging.digismoothie.app/static/media/thumbs_up.7d98f91a.svg",
    },
  ],
};
