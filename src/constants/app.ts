import { Currency } from "@types"

export const offersUrl: string = 'https://private-803503-digismoothietest.apiary-mock.com/offers'
export const defaultCurrency: Currency = 'USD'
export const initialProductAmount: number = 39.99 // dummy product price