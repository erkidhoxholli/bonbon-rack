export interface Offer {
  id: number;
  title: string;
  short_description: string;
  original_price: number;
  discounted_price?: number;
  image: string;
}

export type Currency = "USD" | "EUR" | "CZK" | "PLN"
