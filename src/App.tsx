import { styled } from "styled-components";
import Product from "@components/Product";
import Navbar from "@components/Navbar";

const Wrapper = styled.div``;


function App() {
  return (
    <Wrapper>
      <Navbar />
      <Product />
    </Wrapper>
  );
}

export default App;
