import { defaultCurrency, offersUrl } from "@constants/app";
import { Currency, Offer } from "@types";

interface GetOffersApiResponse {
  offers: Offer[]
  currency: Currency
}

export const getOffers = async (
  signal: AbortSignal
): Promise<GetOffersApiResponse> => {
  try {
    const resp = await fetch(offersUrl, { signal });
    const json = await resp.json();
    return {
      offers: json.offers || [],
      currency: json.currency || defaultCurrency,
    };
  } catch (error) {
    return {
      offers: [],
      currency: defaultCurrency,
    };
  }
};
