# Bonbon Rack 🍬 🚀

https://bonbon-rack.vercel.app/ 

How to run:
```
cd bonbon-rack
npm install
npm run dev
npm run test
```

Icons from https://www.svgrepo.com/

Recommendations: 
* Use FormattedCurrency or create a component to deal with prices/amounts and have consistent UI and internationalization support
* Use .env files to manage base api url for services (now added in constants/)
* Better error handling for api calls (Add Error component and show error message, right now in case there is an error we just return an empty array of offers
* A better way to handle api calls would be to use something like React Query to handle retry, caching, but fetch should work for this simple usecase
* On "loading" scenario in order to avoid CLS maybe use placeholders for each element or optimistic UI with some dummy data
* Definitely adding more tests would be nicer. I just added a few tests for Cart since there we have the most of interactions/calculations. The rest of the components are pretty simple and static
* Its a good idea to keep translations and use some library like react-intl, for for now this should suffice as well :) 
* Lazy load the images and better handling for errors (404 or no image)
* Many more things can be improved, changed so I think it would be a good idea to stop here :)